"""Exercício Programa:
    Faça um programa que:
    1) Pergunta o nome do usuário
    2) Cumprimenta o usuário pelo nome.
    3) Pergunte quantos anos ele tem
    4) Se a idade for menor que 18, ou maior que 100 diz "Acesso negado", caso contrário "Acesso permitido".

    Obs:
    Para comparar um valor obtido via input com um inteiro você vai precisar convertê-lo primeiro para int:
    idade = int(resposta)
"""

# Obtem nome
nome = input("Qual é seu nome?")
print("Olá, ", nome)

# Obtem idade
resposta = input("Qual a sua idade?")
idade = int(resposta)
if idade < 18:
    print("Acesso negado.")
elif idade > 100:
    print("Acesso negado.")
else:
    print("Acesso permitido.")
