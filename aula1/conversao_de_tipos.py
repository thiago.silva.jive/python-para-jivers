# Em alguns momentos, você precisará converter um tipo de um valor para outro.
# Então precisamos fazer conversões no Python. Segue abaixo alguns exemplos

# Verifica tipo de variavel
a = "algum texto"
b = 34
c = 3.141516171819

print(type(c))
print(type(d))
print(type(e))


# conversao para int
a = "10"
print(type(a))
a = int(a)
if type(a) == int:
    print(a)


# conversao para string
a = 10
print(type(a))
a = str(a)
if type(a) == str:
    print(a)


# conversao para float
texto = "23.578"
print(type(texto))
b = float(texto)
if type(b) == float:
    print(b)
