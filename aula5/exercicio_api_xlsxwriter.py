import requests
import json
import xlsxwriter

if __name__ == '__main__':

    #Realizar o request
    resposta_api = requests.get('https://olinda.bcb.gov.br/olinda/servico/RDE_Publicacao/versao/v1/odata/RegistrosPorMes(Mes=@Mes,Ano=@Ano)?%40Mes=02&%40Ano=2020&%24format=json')
    print(resposta_api.text)

    # Obter os documentos da resposta do request e converter para dicionário
    resposta_api = resposta_api.json()
    print(type(resposta_api)) #Veja que agora é um dicionário

    """
    Mas analisando os valores do resposta_api, podemos notar que só existem dois atributos:
    "@odata.contex" - Onde ele define como valor um url
    "value" - Onde possui uma LISTA de dicionários
    
    Então para que a gente pegue os valores que queremos, precisamos pegar o valor do 
    atributo "value", que é uma lista de dicionários
    
    """

    #Obtendo a lista de dicionários
    lista_dicionarios = resposta_api.get('value')


    print(lista_dicionarios)
    print(type(lista_dicionarios))


    #Configurando o xlsxwriter
    arquivo_excel = xlsxwriter.Workbook('dados_api.xlsx')
    planilha = arquivo_excel.add_worksheet()
    bold = arquivo_excel.add_format({'bold': True})


    #Inserindo os títulos de cada coluna
    planilha.write('A1', 'CodigoRDE', bold)
    planilha.write('B1', 'NomePessoaNacional', bold)
    planilha.write('C1', 'UfPessoaNacional', bold)
    planilha.write('D1', 'NomePessoaEstrangeira', bold)
    planilha.write('E1', 'PaisPessoaEstrangeira', bold)
    planilha.write('F1', 'MoedaOperacao', bold)
    planilha.write('G1', 'ValorOperacao', bold)
    planilha.write('H1', 'Sistema', bold)
    planilha.write('I1', 'Ocorrencia', bold)
    planilha.write('J1', 'Modalidade', bold)
    planilha.write('K1', 'Ano', bold)
    planilha.write('L1', 'Mes', bold)


    #Inserir os valores nas colunas
    for i in range(len(lista_dicionarios)):

        planilha.write(i + 1, 0, lista_dicionarios[i].get('CodigoRDE'))
        planilha.write(i + 1, 1, lista_dicionarios[i].get('NomePessoaNacional'))
        planilha.write(i + 1, 2, lista_dicionarios[i].get('UfPessoaNacional'))
        planilha.write(i + 1, 3, lista_dicionarios[i].get('NomePessoaEstrangeira'))
        planilha.write(i + 1, 4, lista_dicionarios[i].get('PaisPessoaEstrangeira'))
        planilha.write(i + 1, 5, lista_dicionarios[i].get('MoedaOperacao'))
        planilha.write(i + 1, 6, lista_dicionarios[i].get('ValorOperacao'))
        planilha.write(i + 1, 7, lista_dicionarios[i].get('Sistema'))
        planilha.write(i + 1, 8, lista_dicionarios[i].get('Ocorrencia'))
        planilha.write(i + 1, 9, lista_dicionarios[i].get('Modalidade'))
        planilha.write(i + 1, 10, lista_dicionarios[i].get('Ano'))
        planilha.write(i + 1, 11, lista_dicionarios[i].get('Mes'))


    arquivo_excel.close()


