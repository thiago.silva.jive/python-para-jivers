import json


def formato_json():
    # Esta é uma string em formato JSON
    json_string = '{"first_name": "Fulano", "last_name":"Oliveira Silva"}'

    print(type(json_string))  # Pode notar que tem tipo str: string


    # Ler o formato json no python
    meu_json = json.loads(json_string)  # uso json.loads para converter a string num formato em que o python consiga ler!

    print(meu_json)
    print(type(meu_json))  # Note que foi convertida para dict: que é um dicionário, formato semelhante ao do json!

    print(meu_json.get("last_name"))  # Use .get('nome do meu parâmetro') para que pegar o valor do parâmetro selecionado!


def formato_dicionario():
    # Criando um dicionário:
    # {} - chaves indicam que é um dicionario
    # {"chave": "valor"} - é um dicionario
    # Em python, o valor pode ser uma string(texto), inteiro, float, lista, ou até mesmo um outro dicionário!

    meu_dicionario = {'titulo1': "algum_valor", 'titulo2': 6, "titulo3": [1, 3, 5, 8]}
    print(meu_dicionario)
    print(type(meu_dicionario))

    # Cria vários dicionários e jogar numa lista
    lista_dicionarios = []
    for i in range(10):
        dict_temporario = {'ID': str(i), 'CEP': str(i+(i*45))}
        lista_dicionarios.append(dict_temporario)

    print(lista_dicionarios)

    # Acessa lista
    for i in range(len(lista_dicionarios)):
        print(lista_dicionarios[i].get('ID'),lista_dicionarios[i].get('CEP'))
    
    # Converte uma lista de dicionários num JSON (USANDO O EXEMPLO ANTERIOR DE LISTA)

    formato_json = json.dumps(lista_dicionarios)
    print(formato_json)
    print(type(formato_json))


def formato_tupla():
    """Tuplas são como listas. Porém são imutáveis. Você não pode alterar valores dentro de uma tupla, nem removê-los, somente navegar por eles."""
    """[] = listas
       () = tuplas  
       {} = dicionários """

    minha_tupla = {"ID": '1', "Nome_pessoa": "Fulano da silva"},{"ID":'12',"Nome_pessoa":"Lorem Ipsum"},{"ID":'3',"Nome_pessoa":"Fausto Silva"}
    print(type(minha_tupla))
    print(minha_tupla)


def dict_para_json():
    #Criar um dicionario
    meu_dicionario = {"ID": '1', "Nome_pessoa": "Fulano da silva"}
    print(type(meu_dicionario))

    #Converter o dicionario para formato JSON
    meu_novo_json = json.dumps(meu_dicionario)
    print(type(meu_novo_json))


def exemplo_dicionario():
    import requests
    response = requests.get('https://google.com', headers={'content-type': 'application/json'})
    print(response.text)


def exemplo_lista_dicionario():
    """Se extraimos dados de um arquivo excel, e armazenamos cada coluna em uma lista"""

    colunaA = ["linha1", "linha2", "linha3", "linha4"]
    colunaB = ["linha1", "linha2", "linha3", "linha4"]
    colunaC = ["linha1", "linha2", "linha3", "linha4"]

    total_colunas = 3
    total_linhas = 4

    #Como obter os valores da segunda linha

    print(colunaA[1])
    print(colunaB[1])
    print(colunaC[1])
    """

    # Se extraimos os dados de um arquivo excel, e armazenamos em um dicionário
    dicionario1 = {"colunaA": ["linha1","linha2","linha3","linha4"], "colunaB": ["linha1","linha2","linha3","linha4"],
                   "colunaC": ["linha1","linha2","linha3","linha4"]}

    # Como obter os valores da segunda linha
    print(dicionario1.get("A")[1])
    print(dicionario1.get("B")[1])
    print(dicionario1.get("C")[1])

    # Ou

    dicionario2 = {"ColunaA":"linha1","ColunaB":"linha1"}, {"ColunaA":"linha2","ColunaB":"linha2"},{"ColunaA":"linha3","ColunaB":"linha3"}

    # Como obter os valores da segunda linha
    print(dicionario2[1])
"""


if __name__ == '__main__':
    formato_json()  # Altere a função por aqui
