import requests
import json

def request_get():
    resposta = requests.get('https://google.com')
    print(resposta)
    print(resposta.status_code)
    print(resposta.text)

    """
    Códigos de resposta:
        Respostas de informação (100-199),
        Respostas de sucesso (200-299),
        Redirecionamentos (300-399)
        Erros do cliente (400-499)
        Erros do servidor (500-599).
    """

def request_get_api():
    resposta = requests.get('https://olinda.bcb.gov.br/olinda/servico/RDE_Publicacao/versao/v1/odata/RegistrosPorMes(Mes=@Mes,Ano=@Ano)?%40Mes=02&%40Ano=2020&%24format=json')
    print(resposta.status_code)
    print(resposta.text)


    # Transformar a resposta em formato JSON (Pois veio como string)
    resposta = resposta.json()

    # Extrair valores do formato JSON
    print(resposta.get('value')[100])


def request_get_com_parametros():
    parametros = {'key1': 'primeiro valor', 'key2': 'algum segundo valor'}
    resposta = requests.get("http://httpbin.org/get", params=parametros)

    print(resposta.text)


def request_post_com_parametros():

    url = 'https://api.github.com/some/endpoint'

    payload = {'some': 'data'}
    headers = {'content-type': 'application/json'}

    resposta = requests.post(url, data=json.dumps(payload), headers=headers)

    print(resposta.text)


if __name__ == '__main__':
    request_get_api()

