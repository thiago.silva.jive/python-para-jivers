import requests
import json

"""Consulta uma lista de endereços usando a API ViaCEP.
https://viacep.com.br/
Os endereços devem estar no formato:
(UF, Município, Logradouro)
"""

enderecos = [
    ("SP", "São Paulo", "Avenida Paulista"),
    ("RJ", "Rio de Janeiro", "Quinta da Boa Vista"),
    ("MG", "Belo Horizonte", "Rua Gustavo da Silveira")
]

# Faz requisições no ViaCEP
CEPs = []
for endereco in enderecos:
    UF, municipio, logradouro = endereco
    response = requests.get("http://viacep.com.br/ws/" + UF + "/" + municipio + "/" + logradouro + "/json/")
    if response.status_code == 200:
        CEP = json.loads(response.text)
        CEPs += CEP
    else:
        print("Houve um erro com a requisição:", response.status_code)

# Imprime resultados na tela
for CEP in CEPs:
    for atributo, valor in CEP.items():
        print(atributo, ":", valor)
    print("")