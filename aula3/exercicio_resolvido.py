from datetime import datetime
from utils import ler_datas

"""
Importe a função ler_datas do pacote utils e use-a para importar a lista de datas do arquivo.
Imprima separadamente a lista de datas anteriores a hoje, e posteriores a hoje, ordenadas cronologicamente.

"""

# Obtem lista do arquivo
datas = ler_datas()
print(datas)
input("")

# Separa as listas em antigas e futuras
datas_antigas = []
datas_futuras = []
hoje = datetime.now()
for data in datas:
    data = datetime.strptime(data, "%d/%m/%Y")
    if data < hoje:
        datas_antigas.append(data)
    elif data > hoje:
        datas_futuras.append(data)
print(datas_antigas)
print(datas_futuras)
input("")

# Ordena as listas
datas_antigas.sort()
datas_futuras.sort()
print(datas_antigas)
print(datas_futuras)
input("")

# Imprime as duas listas
print("Datas antigas:")
for data in datas_antigas:
    data = data.strftime("%d/%m/%Y")
    print(data)
print("Datas futuras:")
for data in datas_futuras:
    data = data.strftime("%d/%m/%Y")
    print(data)
