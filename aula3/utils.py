
def ler_datas():
    datas = []
    with open("datas.txt", "r", encoding="utf-8") as arquivo_datas:
        for data in arquivo_datas:
            datas.append(data.rstrip())
    return datas