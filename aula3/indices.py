# manipulando elementos da lista diretamente
lista = ["ovos", "banana", "chocolate", "leite", "macarrão"]

print("primeiro elemento:", lista[0])  # Lembre-se que o indice comeca no zero!

print("segundo elemento:", lista[1])
lista[1] = "laranja"
print("segundo elemento:", lista[1])

print("último elemento:", lista[-1])
lista.remove("macarrão")
print("último elemento:", lista[-1])
lista.append("grão de bico")
print("último elemento:", lista[-1])


# enumerar os elementos de uma lista
for indice, item in enumerate(lista):
    print(indice, item)


# encontrar o indice de um elemento
if "chocolate" in lista:
    print("chocolate está na posição ", lista.index("chocolate"))
else:
    print("não tem chocolate")


# copiar um segmento da lista (splicing):
sublista = lista[2:-1]  # terceiro elemento ate o penultimo
print(lista)
print("sublista:", sublista)


# quebrar uma string numa lista
alunos_str = "Aline, Bruno, Carlos, Diego"
alunos = alunos_str.split(", ")
print(alunos)


# juntar uma lista numa string
print(";".join(alunos))


# strings como listas
frase = "The quick brown fox jumped over the lazy dogs."
for c in frase:
    print(c, frase.count(c))  # Imprime a contagem de cada letra
print("lazy dogs" in frase)


# Listar a contagem de palavras de um texto, e qual a maior palavra.
zen = """The Zen of Python, by Tim Peters
Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!"""

# Remove quebras de linha, pontos e hifens
zen = zen.replace("\n", " ").replace(".", " ").replace("-", " ")
palavras = zen.split(" ")  # Quebra em lista
while palavras.count('') > 0:  # Remove itens vazios
    palavras.remove('')
print("Número de palavras:", len(palavras))

# Acha a maior palavra
maior_palavra = palavras[0]
for palavra in palavras:
    if len(palavra) > len(maior_palavra):
        maior_palavra = palavra
print("Maior palavra:", maior_palavra)