# Funcões

#1. Como criar uma função

def nomedafuncao ():
    print('Hello world')

#2.Criando uma função hello world
def helloworld():
    print('Hello world')

#5.Criando uma função em que você possa atribuir valores
def soma_valores(valor_a,valor_b):
    resultado = valor_a + valor_b
    print(resultado)

#6.Criando uma função em que você possa atribuir valores e retorna um valor
def calcular_pagamento(qtd_horas, valor_hora): #melhorar o exemplo
  horas = float(qtd_horas)
  taxa = float(valor_hora)
  if horas <= 40:
    salario=horas*taxa
  else:
    h_excd = horas - 40
    salario = 40*taxa+(h_excd*(1.5*taxa))
  return salario



def parsear_texto(texto):
    print("Texto a ser parseado: "+str(texto))


# 3. Crie um if __name__ =='__main__':
"""if __name__ == "__main__" testa se o arquivo de script Python está sendo executado como arquivo principal ou não. 
Isto é útil para evitar certos comportamentos caso seu script seja importado como módulo de outro script."""


if __name__ == '__main__':
    print('Módulo principal criado!')

    #4. Acionando uma função que printa hello world
    helloworld()

    # Acionando a mesma função várias vezes
    for i in range (10):
        helloworld()

    #5.Função que você atribui valor
    soma_valores(10,5)

    #6.Função que retorna um valor
    resultado_retornado = calcular_pagamento(42,20)
    print('Valor retornado pela função: '+str(resultado_retornado))

    #7 Parseia um texto (falta fazer)


