# Podemos importar módulos do python utilizando IMPORT

import time
from datetime import datetime
from datetime import date

#importar uma função que esteja fora deste arquivo

from aula3 import funções
from aula3 import funções as Funções_aula_3


if __name__ == '__main__':


    #Usando datetime para saber a data atual
    data_atual = datetime.now()
    print(data_atual)

    #Usando datetime.date para criar uma data no formato datetime

    data_2 = date(2015,6,1)
    print(data_2)

    #converter uma data para formato datetime
    string_data =  '21/11/2006 16:30'
    data_convertida = datetime.strptime(string_data, "%d/%m/%Y %H:%M")
    print(data_convertida)


    #converter uma data do formato brasileiro para um formato diferente
    data_qualquer = '01/12/2013'
    data_convertida2 = datetime.strptime(data_qualquer, "%d/%m/%Y").strftime("%Y/%m/%d")
    print(data_convertida2)


    print('aguardando 5 segundos ...')
    time.sleep(5) #Usando time.sleep para fazer a aplicação esperar por 5 segundos até que possa prosseguir
    print('pronto!')

    for i in range(10):
        print(i)
        time.sleep(1)

    #Usar uma função que esteja fora do arquivo atual
    pagamento = Funções_aula_3.calcular_pagamento(40,22)
    print("O valor do pagamento deve ser: "+str(pagamento))



