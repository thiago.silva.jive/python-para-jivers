import csv
import json
import requests
from selenium import webdriver
import time
import xlsxwriter

SAVE_PATH = "xlsx/"

def consultar_nomes(lista_de_nomes):
    # Configurar chrome

    # Local onde fica o chromedriver
    CHROMEDRIVER_PATH = "chromedriver.exe"
    # Criando o nosso webdriver com nossas configurações personalizadas
    browser = webdriver.Chrome(executable_path=CHROMEDRIVER_PATH)

    deu_certo = False

    while deu_certo == False:
        try:
            browser.get('http://tucujuris.tjap.jus.br/tucujuris/pages/consultar-processo/consultar-processo.html')
            browser.find_element_by_xpath('//*[@id="form-consulta"]/div/form/div[3]/div/input')
            deu_certo = True
        except:
            deu_certo = False

    time.sleep(2)

    lista_processos = []

    # Loop que pesquisa todos os nomes da lista
    for i in range(len(lista_de_nomes)):
        print('Pesquisando nome: '+str(i) + str(' - ')+str(lista_de_nomes[i]))
        browser.find_element_by_xpath('//*[@id="form-consulta"]/div/form/div[3]/div/input').send_keys(lista_de_nomes[i])
        time.sleep(1)

        #Consulta
        browser.find_element_by_xpath('//input[@id="btnConsultar"]').click()
        time.sleep(4)
        print('Acessando página de resultados')

        #Tenta pegar o primeiro resultado, se não conseguir, passa pro próximo nome a ser consultado.
        try:
            resultado_numero_processo = browser.find_element_by_xpath(
                '//*[@id="resultado-consulta"]/div/div[2]/div[1]/div/a/cabecalhoprocesso/div/div/div[1]/h3/span').text
            lista_processos.append(resultado_numero_processo)

            #Clica no ícone para retornar na página de consulta
            browser.find_element_by_xpath('//*[@id="main-content"]/div/section/ul/li[1]/a').click()
        except:
            # Clica no ícone para retornar na página de consulta
            browser.find_element_by_xpath('//*[@id="main-content"]/div/section/ul/li[1]/a').click()

        #Limpa o campo de pesquisa.
        browser.find_element_by_xpath('//*[@id="form-consulta"]/div/form/div[3]/div/input').clear()

    return lista_processos

def obter_token_escavador():
    HEADERS = {
        "X-Requested-With": "XMLHttpRequest"
    }
    DATA = {
        "username": "email do usuario escavador",
        "password": "senha do usuario escavador"
    }
    response = requests.post("https://api.escavador.com/api/v1/request-token", headers=HEADERS, data=DATA)
    if response.status_code == 200:
        json_response = json.loads(response.text)
        token = json_response["access_token"]
        return token
    else:
        print("Houve um erro na requisicao do token!")
        return False

def obter_mencoes_diario(token, numero_processo):
    HEADERS = {
        "X-Requested-With": "XMLHttpRequest",
        "Authorization": "Bearer " + token
    }
    url = "https://api.escavador.com/api/v1/busca?q=" + numero_processo + "&qo=d"

    response = requests.get(url, headers=HEADERS)
    if response.status_code == 200:
        json_response = json.loads(response.text)
        return json_response.get("items")
    else:
        print("Houve um erro na requisicao do token!")
        return False

def dicts_para_lista(dicts):
    lista = []
    labels = []
    for key in dicts[0].keys():
        labels.append(key)
    lista.append(labels)
    for dic in dicts:
        registro = []
        for label in labels:
            registro.append(dic[label])
        lista.append(registro)
    return lista


def salvar_para_xlsx(filepath, registros):
    arquivo_excel = xlsxwriter.Workbook(filepath)
    planilha = arquivo_excel.add_worksheet()
    for linha, registro in enumerate(registros):
        for coluna, valor in enumerate(registro):
            planilha.write(linha, coluna, valor)
    arquivo_excel.close()
    print("Arquivo", filepath, "salvo")



if __name__ == '__main__':
    print('initializing')

    #criar lista de nomes
    lista_nomes = []

    #Part 1 - Abrir o CSV
    with open('lista_nomes.csv', encoding='utf-8') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=';')
        for row in readCSV:
            lista_nomes.append(row[0])

    #Ver o que tem na lista
    print(lista_nomes)

    #Parte 2: Web Scraping
    #Criar uma função que colete os números de processo e retorne uma lista de processos
    lista_processos = consultar_nomes(lista_nomes)
    print(lista_processos)

    #Parte 3: Consulta API
    token = obter_token_escavador()
    if token:
        for numero_processo in lista_processos:
            file_path = SAVE_PATH + numero_processo + ".xlsx"
            mencoes_diario = obter_mencoes_diario(token, numero_processo)
            if mencoes_diario:
                linhas = dicts_para_lista(mencoes_diario)
                salvar_para_xlsx(file_path, linhas)
            else:
                print("Processo", numero_processo, "não tem menções no diário oficial.")
