from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

PESQUISA = "Tornado"

# Inicializa chrome
CHROMEDRIVER_FILEPATH = "c:\\chromedriver.exe"
"""
DOWNLOAD_PATH = "C:\\downloads\\"

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--start-maximized")
chrome_options.add_argument("--headless")
prefs = {"profile.default_content_settings.popups": 0,
		 "download.default_directory": DOWNLOAD_PATH
		 "directory_upgrade": True}
chrome_options.add_experimental_option("prefs", prefs)
browser = webdriver.Chrome(options=chrome_options, executable_path=CHROMEDRIVER_FILEPATH)"""

driver = webdriver.Chrome(CHROMEDRIVER_FILEPATH)
driver.maximize_window()

# Abre site
driver.get("https://www.cnnbrasil.com.br/")

# Busca
driver.find_element_by_class_name("ico_search").click()
campo_busca = driver.find_element_by_class_name("search-input")
campo_busca.click()
campo_busca.send_keys(PESQUISA, Keys.RETURN)
time.sleep(10)

manchetes = []
links = []
# Extrai manchetes
divs = driver.find_elements_by_class_name("search-item__title")
for div in divs:
    manchete = div.text
    link = div.find_element_by_xpath(".//ancestor::a").get_attribute("href")
    if manchete not in manchetes:
        manchetes.append(manchete)
        links.append(link)
        print(manchete)
        print(link, "\n")

# Finaliza
print(manchetes)
driver.quit()