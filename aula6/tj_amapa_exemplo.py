from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options



if __name__ == '__main__':

    # Configurar chrome

    #Local onde fica o chromedriver
    CHROMEDRIVER_PATH = "chromedriver.exe"

    #Local onde fica a pasta de downloads
    DOWNLOAD_PATH = "C:\\Users\\thiago.silva\\pasta_curso_jivers\\aula6\\aula6_chromedownloads\\"

    chrome_options = Options()
    preferences = {"download.default_directory": DOWNLOAD_PATH,
                   "directory_upgrade": True,
                   "safebrowsing.enabled": True}
    chrome_options.add_experimental_option("prefs", preferences)
    #chrome_options.add_argument('--headless') #Use se quiser rodar em background

    #Criando o nosso webdriver com nossas configurações personalizadas
    browser = webdriver.Chrome(options=chrome_options, executable_path=CHROMEDRIVER_PATH)



    # Inicia o chrome no tj amapá
    browser.get('https://www.tjap.jus.br/portal/')
    print('Abrindo chrome')


    time.sleep(3)

    #Clica em buscar em consulta rápida, para ser redirecionado pra página de busca.
    browser.find_element_by_xpath('//*[@id="aside"]/div/div[1]/div/div/div/form[1]/div[2]/input[1]').click()

    time.sleep(3)

    browser.find_element_by_xpath('//*[@id="form-consulta"]/div/form/div[3]/div/input').send_keys('Caio da silva')
    time.sleep(1)

    browser.find_element_by_xpath('//*[@id="form-consulta"]/div/form/div[5]/div/label[4]/input').click()

    browser.find_element_by_xpath('//*[@id="btnConsultar"]').click()
    time.sleep(4)
    print('Acessando página de resultados')

    #Começar a extração de dados

    #Obter todas as informações do primeiro resultado:
    resultado_numero_processo = browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div[1]/div/a/cabecalhoprocesso/div/div/div[1]/h3/span').text
    resultado_primeira_linha = browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div[1]/div/a/cabecalhoprocesso/div/div/p/span[1]').text
    resultado_segunda_linha = browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div[1]/div/a/cabecalhoprocesso/div/div/p/span[3]').text
    resultado_terceira_linha = browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div[1]/div/a/cabecalhoprocesso/div/div/p/span[4]').text
    resultado_quarta_linha = browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div[1]/div/a/cabecalhoprocesso/div/div/div[2]/div/p[1]/span[2]').text
    resultado_quinta_linha = browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div[1]/div/a/cabecalhoprocesso/div/div/div[2]/div/p[2]/span[2]').text

    print('\n\n\n')
    print(resultado_numero_processo)
    print(resultado_primeira_linha)
    print(resultado_segunda_linha)
    print(resultado_segunda_linha)
    print(resultado_terceira_linha)
    print(resultado_quarta_linha)
    print(resultado_quinta_linha)


    # Extrair todos os resultados com loop

    print('\n\n Extraindo todos os resultados ...')
    time.sleep(2)

    #Loop por cada bloco
    quantidade_total_blocos = browser.find_elements_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div')
    print(len(quantidade_total_blocos))

    for i in range(len(quantidade_total_blocos)):

        try:
            print('Número do loop: '+str(i))
            browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div[8]')
            browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div['+str(i+1)+']/div/a/cabecalhoprocesso/div/div/div[1]/h3/span')


            resultado_numero_processo = browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div['+str(i+1)+']/div/a/cabecalhoprocesso/div/div/div[1]/h3/span').text
            resultado_primeira_linha = browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div['+str(i+1)+']/div/a/cabecalhoprocesso/div/div/p/span[1]').text
            resultado_segunda_linha = browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div['+str(i+1)+']/div/a/cabecalhoprocesso/div/div/p/span[3]').text
            resultado_terceira_linha = browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div['+str(i+1)+']/div/a/cabecalhoprocesso/div/div/p/span[4]').text
            resultado_quarta_linha = browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div['+str(i+1)+']/div/a/cabecalhoprocesso/div/div/div[2]/div/p[1]/span[2]').text
            resultado_quinta_linha = browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div['+str(i+1)+']/div/a/cabecalhoprocesso/div/div/div[2]/div/p[2]/span[2]').text

            print('\n\n\n')
            print(resultado_numero_processo)
            print(resultado_primeira_linha)
            print(resultado_segunda_linha)
            print(resultado_segunda_linha)
            print(resultado_terceira_linha)
            print(resultado_quarta_linha)
            print(resultado_quinta_linha)
        except:
            pass

    #baixar todos os documentos
    for i in range(len(quantidade_total_blocos)):

        try:
            print('Número do loop de download: '+str(i))
            browser.find_element_by_xpath('//*[@id="resultado-consulta"]/div/div[2]/div['+str(i+1)+']/div/a/cabecalhoprocesso/div/div/div[1]/h3/small[3]/resumoprocesso/a/i').click()
            time.sleep(3)
        except:
            pass

    #Fechar o webdriver

    browser.quit()