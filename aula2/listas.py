### Listas

# Durante o desenvolvimento de software, independentemente de plataforma e linguagem, é comum a necessidade de lidar com listas.
# Por exemplo, elas podem ser empregadas para armazenar contatos telefônicos ou tarefas.
# Uma lista é uma estrutura de dados composta por itens organizados de forma linear, na qual cada um pode ser acessado a partir de um índice,
# que representa sua posição na coleção (iniciando em zero).


# CRIANDO LISTAS

lista = []

#atribuir valor a lista
lista = ['São Paulo','Rio de Janeiro', 'Minas Gerais', 'Paraná']
print(lista)

#Adicionar valores nesta lista
lista.append('Nova Cidade')
print(lista)

#Verificar se é mesmo uma lista: type(lista)
print("Tipo: "+ str(type(lista)) )

#verificar quantos itens tem nessa lista: len(lista)
print("Essa lista tem "+ str(len(lista)) + " itens")

#contagem de objetos na lista: .count()
print("Contagem de valores com 'São Paulo': "+ str(lista.count('São Paulo')))

#Encontrar o primeiro e terceiro valor da lista: lista[posição]
print("O primeiro valor da lista é: "+str(lista[0]))
print("O terceiro valor da lista é: "+str(lista[2]))

#Minimo, máximo e soma de uma lista numérica: min() max() sum()

lista_numerica = [14,11,55,43,23,76,33,2,455]
print("O valor mínimo da lista numérica é: "+str(min(lista_numerica)))
print("O valor máximo da lista numérica é: "+str(max(lista_numerica)))
print("A soma da lista numérica é: "+str(sum(lista_numerica)))


#Removendo valores da lista: .remove()
lista.remove('São Paulo')
print("Remoção de itens: contagem de valores com 'São Paulo': "+ str(lista.count('São Paulo')))


#Ordenando valores da lista: .sort()
print("Lista desorganizada: "+ str(lista))
lista.sort()
print("Lista organizada: "+ str(lista))


#Invertendo valores da lista: .reverse()
print("Lista organizada: "+ str(lista))
lista.reverse()
print("Lista invertida: "+ str(lista))



#Limpando toda a lista: .clear()
lista.clear()
print("Valores dentro da lista após ser limpa: "+ str(lista))

