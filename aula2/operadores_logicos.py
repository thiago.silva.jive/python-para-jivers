# and
A = True
B = False
print("Valor de A and B: ", A and B)


# or
A = True
B = False
print ("Valor de A or B: ", A or B)


# not
A = True
if not A:
    print('alguma coisa')


# exemplo
idade = 19
tem_documento = True

if idade < 18 or not tem_documento:
    print("Acesso negado.")

if idade >= 18 and tem_documento:
    print("Acesso permitido.")








