"""Exemplos de loops"""

# for num intervalo
for numero in range(100):  # vai de 0 a 99, o último não entra no intervalo
    print(numero)


# for numa lista e continue: imprime numeros da lista exceto o 13
lista = [1, 4, 7, 5, 13, 8]
for numero in lista:
    if numero == 13:
        continue
    print(numero)


# soma 5 numeros:
soma = 0
for n in range(5):
    resposta = input("Digite o número:")
    soma = soma + int(resposta)
print("Soma = ", soma)


# while
contador = 1
while contador < 1000000:
    contador = contador + 1


# while e break: Digita a senha
resposta = input("Digite a senha:")

while resposta != "a senha":
    print("Errou! Errou feio, errou rude!")
    resposta = input("Digite a senha:")

    if resposta == "" or resposta == "desisto":
        print("Desistiu? Seu fanfarrão!")
        break


# loop infinito: cuidado!
while True:
    if input("Qual é a safeword?") == "Rumpelstiltskin":
        break