"""Alguns exemplos de manipulações com strings."""


# Concatenação
logradouro = "Av. Brg. Faria Lima"
numero = 1485
complemento = "18° andar"

endereco = logradouro + ", " + str(numero) + ". " + complemento + "."
print("O endereço é", endereco)


# replace
price_str = "US$ 1,294,222.24"
price_str = price_str.replace('US$','') # remove US$
price_str = price_str.replace(",", "") #remove as vírgulas
price = float(price_str)
print(price)



# startswith e endswith
nome_arquivo = "comprovante_itau.csv"
if nome_arquivo.startswith("comprovante_") and nome_arquivo.endswith(".csv"):
    print("Arquivo carregado.")
else:
    print("Arquivo inválido.")


# replace
price_str = "US$ 1,294,222.24"

price_str = price_str.replace('US$','') # remove US$
price_str = price_str.replace(",", "") #remove as vírgulas
price = float(price_str)
print(price)