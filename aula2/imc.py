"""Calcula o IMC
IMC = peso / altura² """

# Obtem peso
resposta = input("Qual é seu peso? ")
peso = float(resposta)

# Obtem altura
resposta = input("Qual é sua altura?")
altura = float(resposta)

# Calcula IMC
IMC = peso / (altura*altura)
print("Seu IMC é", IMC)

if IMC <= 18.5:
    print("Seu IMC está abaixo do ideal.")
elif IMC > 18.5 and IMC < 25:
    print("Seu IMC está na faixa ideal.")
else:
    print("Seu IMC está acima do ideal.")