"""
1. Faça um programa que recebe 5 números do usuário, insere numa lista e calcula a média.
"""
lista = []
for n in range(5):
    resposta = input("Digite um numero:")
    lista.append(float(resposta))
media = sum(lista)/len(lista)
print("media:", media)

"""
2. Faça um programa que avisa se uma lista tem valores repetidos.
"""

lista = [2, 5, 7, 8, 8, 0]
for n in lista:
    if lista.count(n) > 1:
        print("A lista tem valores repetidos.")
        break
