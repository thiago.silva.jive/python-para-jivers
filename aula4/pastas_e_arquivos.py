import os


def pasta_atual():
    # pasta atual
    print(os.getcwd())


def mudar_pasta_atual(diretorio):
    # muda pasta atual
    os.chdir(diretorio)
    print(os.getcwd())


def verificar_pasta_arquivos(diretorio):
    # verificando se pasta ou arquivo atual existe
    if os.path.exists(diretorio):
        print("caminho existe")


def listar_arquivos(arquivo):
    # listar arquivos
    print(os.listdir(arquivo))


def mover_renomear(arquivo1, arquivo2):
    # movendo / renomeando
    import shutil
    shutil.move(arquivo1, arquivo2)


def deletar_arquivos(arquivo1, arquivo2):
    import shutil
    shutil.copyfile(arquivo1, arquivo2)


def deletar_arquivos(arquivo):
    # deletando
    import os
    # deleta arquivo
    os.remove(arquivo)


def deletar_pasta_vazia(diretorio):
    # deleta pasta vazia.
    os.rmdir(diretorio)


def deletar_tudo_da_pasta(diretorio):
    import shutil
    # deleta pasta e tudo dentro dela
    shutil.rmtree(diretorio)


def ler_arquivos(diretorio):
    # leitura
    with open(diretorio, "r", encoding="utf-8") as arquivo:
        for line in arquivo:
            print(line.rstrip())


if __name__ == '__main__':
    pasta_atual()
