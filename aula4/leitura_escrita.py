# Escrita
dados = ['linha 1', 'linha 2']
with open("arquivo.txt", "w+", encoding="utf-8") as arquivo:
    for dado in dados:
        arquivo.write(dado + "\n")

# use a+ em vez de w para adicionar ao conteudo já existente
# use wb para dados binários

# Leitura
with open("arquivo.txt", "r", encoding="utf-8") as arquivo:
    for line in arquivo:
        print(line.rstrip())  # rstrip remove espaços e quebra de linha no final

# use rb para dados binários
