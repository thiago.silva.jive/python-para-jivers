"""XlsxWriter só pode criar novos documentos. Não consegue ler ou modificar xlsx existentes"""

import xlsxwriter


def criar_tabela(lista_colunaA, lista_colunaB):
    # Cria um arquivo em excel e cria uma planilha
    workbook = xlsxwriter.Workbook('exemplo_xlsxwriter.xlsx')
    planilha = workbook.add_worksheet("Dados")

    # Aumentar o espaçamento das  colunas para o texto ficar mais visível
    planilha.set_column('A:A', 20)
    planilha.set_column('B:B', 20)

    # Adicionar o formato negrito
    bold = workbook.add_format({'bold': True})

    # Escrever alguma coisa (no caso o título das colunas)
    planilha.write('A1', 'Produto', bold)
    planilha.write('B1', 'Preço', bold)
    planilha.write('C1', 'OUTRA COLUNA', bold)

    # Escrevendo nas linhas da coluna A tudo o que estiver na lista
    for i in range(len(lista_colunaA)):
        planilha.write(i + 1, 0, lista_colunaA[i])

    # Adicionando o formato MONEY
    money_format = workbook.add_format({'num_format': '[$R$ ]#,##0.00'})

    # Escrevendo nas linhas da coluna B tudo o que estiver na lista
    for i in range(len(lista_colunaB)):
        planilha.write(i + 1, 1, lista_colunaB[i], money_format)

    # Somando os valores da coluna B (manualmente através de um loop)
    valor_soma = 0
    for i in range(len(lista_colunaB)):
        valor_soma = valor_soma + lista_colunaB[i]

    # No final da planilha, adicionar "total" na coluna A, e soma dos resultados na coluna B
    planilha.write(int(len(colunaA) + 1), 0, 'Total', bold)
    planilha.write(int(len(colunaA) + 1), 1, valor_soma, money_format)


    # Inserir uma imagem
    planilha.insert_image('E5', 'jive.jpg')


    workbook.close()


if __name__ == '__main__':
    print('xlsxwriter')

    colunaA = ['A', 'B', 'C', 'D', 'E']
    colunaB = [2342, 3215, 124, 564, 654]

    criar_tabela(colunaA, colunaB)
