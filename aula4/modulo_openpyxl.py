import openpyxl

def ler_arquivo():
    workbook = openpyxl.load_workbook(filename="exemplo_xlsxwriter.xlsx")
    sheet = workbook.active


    for i in range(sheet.max_column):
        for j in range(sheet.max_row):
            print(sheet.cell(row=j+1, column=i+1).value)

def escrever_arquivo():
    workbook = openpyxl.Workbook()
    sheet = workbook.active

    data = (
        ("Idade", 'Peso', 'Valor'),
        (11, 48, 50),
        (81, 30, 82),
        (20, 51, 72),
        (21, 14, 60),
        (28, 41, 49),
        (74, 65, 53)

    )

    #Escreve os valores
    for i in data:
        sheet.append(i)

    #Outro exemplo de como adicionar valores nas tabelas
    sheet['A8'] = 87
    sheet['A9'] = "Algum valor"
    sheet['A10'] = 'AAAAAAA'
    sheet['A11'] = 'BBBBBBB'

    #Local onde iremos salvar
    workbook.save(filename="openpyxl.xlsx")

if __name__ == '__main__':

    ler_arquivo()

