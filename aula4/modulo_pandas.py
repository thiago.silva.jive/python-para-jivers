import pandas as pd

"""
Demonstração Pandas.

Instalação
pip install pandas matplotlib openpyxl xlrd xlsxwriter

Veja mais:
Tutorial
https://www.learndatasci.com/tutorials/python-pandas-tutorial-complete-introduction-for-beginners/

Cheat sheet
https://www.datacamp.com/community/blog/python-pandas-cheat-sheet
"""

# Le csv
dados = pd.read_csv("dados.csv", sep='\t')

# Prepara coluna Preço
dados['Preço'] = dados['Preço'].str.strip()  # Remove espaços extras
dados['Preço'] = dados['Preço'].str.replace(',', '.')  # Troca , por .
dados['Preço'] = dados['Preço'].astype(float)  # Converte para float

# Ordena
dados = dados.sort_values(by='Preço')

# Calcula soma
soma = dados['Preço'].sum()
dados = dados.append([{'Produto':'Total', 'Preço': soma}], ignore_index=True)  # Anexa linha no dataframe

# Formata coluna preços
dados['Preço_formatado'] = dados['Preço'].map('R${:,.2f}'.format)

# Exporta dataframe para excel
dados.to_excel("output.xlsx", index=False)

# # Exporta para Excel (permite escolher a planilha)
# writer = pd.ExcelWriter('output.xlsx', engine='xlsxwriter')

# # Write each dataframe to a different worksheet.
# dados[['Produto', 'Preço_formatado']].to_excel(writer, sheet_name='Dados', index=False)
# dados.to_excel(writer, sheet_name='Gráfico', index=False)

# # Close the Pandas Excel writer and output the Excel file.
# writer.save()



# Prepara dados para plotagem do grafico de pizza
# Converte pra lista
labels = dados['Produto'].tolist()
sizes = dados['Preço'].tolist()
# Remove último elemento (soma)
labels.pop(), sizes.pop()

# Importa matplotlib para graficos
import matplotlib.pyplot as plt

plt.pie(sizes, labels=labels, autopct='%1.1f%%', shadow=True, startangle=90)

#plt.show()
plt.savefig("pizza.png", dpi = 150)


# Importa openpyxel para adicionar gráfico na planilha
import openpyxl

wb = openpyxl.load_workbook("output.xlsx")
wb.create_sheet("Gráfico")
ws = wb["Gráfico"]

img = openpyxl.drawing.image.Image("pizza.png")
img.anchor = "A2"

ws.add_image(img)
wb.save("output.xlsx")